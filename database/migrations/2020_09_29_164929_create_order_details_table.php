<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->id('order_id');
            $table->id('product_id');

            $table->tinyInteger('status')->comment('reserve for future');
            $table->Integer('price');
            $table->Integer('quantity');
            $table->Integer('discount')->comment('discount code');

            $table->timestamps();
            $table->softDeletes()->comment('deleted at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
