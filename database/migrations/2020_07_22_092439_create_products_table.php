<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->id();

            $table->unsignedBigInteger('category_id')
                  ->comment('products table belongs to categories table');
            $table->foreign('category_id')->on('categories')->references('id');

            $table->string('title', 100);
            $table->longText('description');
            $table->string('icon')->comment('pathname');

            $table->integer('quantity')->comment('number of available products');
            $table->bigInteger('price');
            $table->float('discount');
            $table->tinyInteger('status')
                  ->comment('status is 1 when a product is available and it is 0 otherwise.');

            $table->timestamps();
            $table->softDeletes()->comment('deleted at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {

        Schema::dropIfExists('products');

    }
}
