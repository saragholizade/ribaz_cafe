<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->id();
            $table->id('user_id');
          //  $table->id('payment_id');

            $table->tinyInteger('status')->comment('reserve for future');
            $table->Integer('order_number');
            $table->tinyInteger('transact_status', [1,0])->comment('0 for unpaid orders and 1 for paid orders');

            $table->timestamps();
            $table->softDeletes()->comment('deleted at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
