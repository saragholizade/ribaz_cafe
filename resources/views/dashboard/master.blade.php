<html>
<head>
    <title>App Name - @yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="{{{ URL::asset('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css')}}}"
          rel="stylesheet" media="all">
</head>
<body>

<div class="sidenav">
    @section('sidebar')

        <style>
            .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
                width: 100%;
                padding-right: 0 !important;
                padding-left: 0 !important;
                margin-right: auto;
                margin-left: auto;
            }

            body {
                direction: rtl !important;
                font-family: "B Roya";
            }

            .sidebar {
                width: 12%;
                height: 1000px;
                background-color: #666666;
                display: inline-block;
                float: right;
            }

            .sidebar span a {
                display: block;
                padding: 15px;
                color: #fff;
                text-decoration: none;
                font-size: 20px;
                text-align: center;
                margin: 20px;
                display: block;

            }

            .sidebar a:hover {
                color: #000000;
            }

            .content {
                width: 85%;
                height: 1000px;
                display: inline-block;
            }

            /*.content a {*/
            /*    text-decoration: none;*/
            /*}*/

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            th, td {
                text-align: center;
                padding: 16px;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2;
            }

        </style>

        <div class="sidebar">



                @if(Auth::user()->level=='admin')

                    <button class="dropdown-btn">{{Auth::user()->first_name }}
                        <i class="fa fa-caret-down"></i>
                    </button>

                    <div class="dropdown-container">
                        <a class="dropdown-item" href="{{route('main')}}"
                           onclick="event.preventDefault(); document.getElementById('main-form').submit();">
                            بازگشت به صفحه اصلی سایت
                        </a>
                        <form id="main-form" action="{{ route('main') }}" method="GET" style="display: none;">
                            @csrf
                        </form>

                        <a class="dropdown-item " href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('fields.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                @endif

            <span><a href="{{route('dashboard')}}">داشبورد</a></span>
            <span><a href="{{route('category.show')}}">دسته بندی</a></span>
            <span><a href="{{route('products.show')}}">محصولات</a></span>
            <span><a href="{{route('users')}}">کاربران</a></span>
           <span><a href="{{route('order_details')}}">سفارشات</a></span>

        </div>
    @show

    <div class="content">
        @yield('content')
    </div>
</div>

</body>
</html>
