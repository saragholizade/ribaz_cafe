@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="container marketing">

    <div  style="margin: 15px">
        <a class="btn btn-primary" href="{{route('category.create')}}">{{__('validation.attributes.add-new-category')}}</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>{{__('validation.attributes.title')}}</th>
            <th>{{__('validation.attributes.icon')}}</th>
            <th>{{__('validation.attributes.operation')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $key=>$item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->title}}</td>
                <td>
                    <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">
                </td>
                <td>{{$item->operations}}
                    <span><a
                            href="{{route('category.edit',['category_id'=>$item->id])}}">{{__('validation.attributes.edit')}}</a></span>
                    |
                    <span><a href="{{route('category.delete', ['category_id'=>$item->id])}}"
                             style="color:#e60000">{{__('validation.attributes.delete')}}</a></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('dashboard.pagation_default', ['paginator' => $category])
    </div>
@endsection


