@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div style="margin: 20px">

        <a class="btn btn-primary"
           href="{{route('product.create')}}">{{__('validation.attributes.add-new-product')}}</a>
    </div>
    <div class="container marketing">
        <div>
            <table class="table table-striped">
                <thead class="col">
                <tr>
                    <th>#</th>
                    <th>{{__('validation.attributes.title')}}</th>
                    <th>{{__('validation.attributes.category')}}</th>
                    <th>{{__('validation.attributes.icon')}}</th>
                    <th>{{__('validation.attributes.quantity')}}</th>
                    <th>{{__('validation.attributes.price')}}</th>
                    <th>{{__('validation.attributes.discount')}}</th>
                    <th>{{__('validation.attributes.status')}}</th>
                    <th>{{__('validation.attributes.operation')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->getCategory->title}}</td>
                        <td>
                            <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">
                        </td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->discount}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{$item->operations}}
                            <span><a
                                    href="{{route('product.edit',['product_id'=>$item->id])}}">{{__('validation.attributes.edit')}}</a></span>
                            |
                            <span><a href="{{route('product.delete', ['product_id'=>$item->id])}}"
                                     style="color:#e60000">{{__('validation.attributes.delete')}}</a></span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @include('dashboard.pagation_default', ['paginator' => $products])
    </div>
@endsection


