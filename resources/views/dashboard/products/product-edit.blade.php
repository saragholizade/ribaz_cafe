@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 100px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
        }

        label{
            float: right;
        }
    </style>
    <div class="box">
        <div class="in-box form-group">
            <form action="{{url('products/'.$product_id->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')

                <select class="form-control" name="category_id" id="">
                    <option value="" selected>انتخاب مجموعه</option>
                    @foreach($category as $item)
                        <option value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                </select>
                @error('category_id')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="title">{{__('validation.attributes.title')}}</label>
                <input class="form-control" type="text" name="title" value="{{$product_id->title}}" >
                @error('title')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="description">{{__('validation.attributes.description')}}</label>
                <textarea class="form-control" name="description" id="" cols="30" rows="3"
                          placeholder="توضیحات"></textarea>
                @error('description')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <div style="float: right">
                    <label for="files" class="btn-sm btn-info">{{__('validation.attributes.select-icon')}}</label>
                    <input data-preview="#preview" id="files" style="visibility:hidden;" type="file" name="icon" value="{{$product_id->icon}}">
                    @if($product_id->icon)
                        <img id="preview" src="{{asset('storage/'.$product_id->icon)}}" style="width:200px;height:200px;">
                    @endif
                </div>
                @error('icon')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br><br>

                <label for="quantity">{{__('validation.attributes.quantity')}}</label>
                <input class="form-control" type="text" name="quantity" value="{{$product_id->quantity}}" >
                @error('quantity')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="price">{{__('validation.attributes.price')}}</label>
                <input class="form-control" type="text" name="price" value="{{$product_id->price}}" >
                @error('price')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="discount">{{__('validation.attributes.discount')}}</label>
                <input class="form-control" type="text" name="discount" value="{{$product_id->discount}}" >
                @error('discount')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <select class="form-control" name="status" id="">
                    <option value="">وضعیت</option>
                    <option value="1">فعال</option>
                    <option value="0">غیر فعال</option>
                </select>
                @error('status')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">{{__('validation.attributes.edit')}}</button>
            </form>

{{--            <form action="{{route('product.delete', ['product_id'=>$product_id->id])}}" method="post">--}}
{{--                @method('delete')--}}
{{--                <button type="submit" class="btn btn-danger">{{__('validation.attributes.delete')}}</button>--}}
{{--            </form>--}}

        </div>
    </div>

@endsection

