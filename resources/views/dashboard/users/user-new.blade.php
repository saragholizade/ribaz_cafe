@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 100px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
        }

        form label {
            float: right;
        }
    </style>
    <div class="box">
        <div class="in-box form-group">
            <form action="{{url('users')}}" method="post">
                @method('POST')
                <label for="first_name">{{__('validation.attributes.first_name')}}</label>
                <input class="form-control" type="text" name="first_name" placeholder="نام">
                @error('first_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="last_name">{{__('validation.attributes.last_name')}}</label>
                <input class="form-control" type="text" name="last_name" placeholder="نام خانوادگی">
                @error('last_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="user_name">{{__('validation.attributes.user_name')}}</label>
                <input class="form-control" type="text" name="user_name" placeholder="نام کاربری">
                @error('user_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="password">{{__('validation.attributes.password')}}</label>
                <input class="form-control" type="text" name="password" placeholder="پسورد">
                @error('password')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="email">{{__('validation.attributes.email')}}</label>
                <input class="form-control" type="text" name="email" placeholder="ایمیل">
                @error('email')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="phone_number">{{__('validation.attributes.phone_number')}}</label>
                <input class="form-control" type="text" name="phone_number">
                @error('phone_number')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="status">{{__('validation.attributes.status')}}</label>
                <select class="form-control" name="status" id="">
                    <option value="">{{__('validation.attributes.status')}}</option>
                    <option value="1" @if( old('status') == '1' ) selected="selected" @endif>{{__('validation.attributes.active')}}</option>
                    <option value="0" @if( old('status') == '0' ) selected="selected" @endif>{{__('validation.attributes.de-active')}}</option>
                </select>
                @error('status')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>
                <label for="level">{{__('validation.attributes.level')}}</label>
                <select class="form-control" name="level" id="">
                    <option value="">{{__('validation.attributes.level')}}</option>
                    <option value="admin" @if( old('level') == 'admin' ) selected="selected" @endif>{{__('validation.attributes.admin')}}</option>
                    <option value="user" @if( old('level')  == 'user') selected="selected" @endif>{{__('validation.attributes.user')}}</option>
                </select>
                @error('level')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">{{__('validation.attributes.insert')}}</button>
            </form>
        </div>
    </div>

@endsection
