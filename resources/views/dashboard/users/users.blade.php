@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="container">

    <div class="container marketing" style="margin: 20px">

        <a class="btn btn-primary" href="{{route('user.create')}}">ایجاد کاربر جدید</a>
    </div>

    <table class="table table-striped">
        <thead class="col">
        <tr>
            <th>#</th>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>نام کاربری</th>
            <th>ایمیل</th>
            <th>تلفن همراه</th>
            <th>سطح دسترسی</th>
            <th>وضعیت</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->first_name}}</td>
                <td>{{$item->last_name}}</td>
                <td>{{$item->user_name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->phone_number}}</td>
                <td>{{$item->level}}</td>
                <td>{{$item->status}}</td>
                <td>{{$item->operations}}
                    <span><a href="{{route('user.edit',['user_id'=>$item->id])}}">ویرایش</a></span> |
                    <span><a href="{{route('user.delete', ['user_id'=>$item->id])}}" style="color:#e60000">حذف</a></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection


