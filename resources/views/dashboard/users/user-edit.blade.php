@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 100px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
        }

        label{
            float: right;
        }
    </style>
    <div class="box">
        <div class="in-box form-group">
            <form action="{{url('users/'.$user_id->id)}}" method="post">
                @method('PUT')
                <label for="">نام</label>
                <input class="form-control" type="text" name="first_name" value="{{$user_id->first_name}}">
                @error('first_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="">نام خانوادگی</label>
                <input class="form-control" type="text" name="last_name" value="{{$user_id->last_name}}">
                @error('last_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="">نام کاربری</label>
                <input class="form-control" type="text" name="user_name" value="{{$user_id->user_name}}">
                @error('user_name')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="">پسورد</label>
                <input class="form-control" type="text" name="password" value="{{$user_id->password}}">
                @error('password')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="">ایمیل</label>
                <input class="form-control" type="text" name="email" value="{{$user_id->email}}" disabled><br>

                <label for="">تلفن همراه</label>
                <input class="form-control" type="text" name="phone_number" value="{{$user_id->phone_number}}" disabled><br>

                <select class="form-control" name="status" id="">
                    <option value="">{{__('validation.attributes.status')}}</option>

                    <option value="1" @if( old('status') == '1' ) selected="selected" @endif>
                        {{__('validation.attributes.active')}}</option>

                    <option value="0" @if( old('status') == '0' ) selected="selected" @endif>
                        {{__('validation.attributes.de-active')}}</option>
                </select>
                @error('status')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <select class="form-control" name="level" id="">
                    <option value="">{{__('validation.attributes.level')}}</option>

                    <option value="admin" @if( old('level') == 'admin' ) selected="selected" @endif>
                        {{__('validation.attributes.admin')}}</option>

                    <option value="user" @if( old('level')  == 'user') selected="selected" @endif>
                        {{__('validation.attributes.user')}}</option>
                </select>
                @error('level')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">ویرایش</button>
            </form>

{{--            <form action="{{route('user.delete', ['user_id'=>$user_id->id])}}" method="post">--}}
{{--                @method('delete')--}}
{{--                <button type="submit" class="btn btn-danger">حذف کاربر</button>--}}
{{--            </form>--}}
        </div>
    </div>

@endsection
