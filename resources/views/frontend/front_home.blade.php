@extends('frontend.mainlayout')

@section('content')
    @include('frontend.partials.header2')

    <div class="container marketing">

        <div class="row">
            @foreach($category as $item)
                <div class="col-lg-4">
                    <img class="rounded-circle"
                         src="{{asset('storage/'.$item->icon)}}"
                         alt="Generic placeholder image" width="140" height="140">
                    <h5 class="mt-2">{{$item->title}}</h5>
                    <p><a class="btn btn-secondary" href="#" role="button">مشاهده جزئیات </a></p>
                </div>
            @endforeach
        </div>

    </div>
    <div class="album py-5 bg-light">
        <div class="container">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h5">نوشیدنی های سرد</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group ml-2">
                        <button class="btn btn-sm btn-outline-secondary">بیشتر</button>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($productGroup1 as $product)
                    <div class="col-12 col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top"
                                 src="{{asset('storage/'.$product->icon)}}"
                                 alt="Card image cap" width="140" height="140">
                            <div class="card-body">
                                <h5><a class="card-text" href="{{route('product.details' , ['product'=>$product->id] )  }}" role="button">{{$product->title}}</a></h5>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary"><a href="{{route('product.add_to_cart',$product->id)}}">خرید</a></button>
                                    </div>
                                    <small class="text-muted">{{$product->price .'تومان'}}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h5">نوشیدنی های گرم</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group ml-2">
                        <button class="btn btn-sm btn-outline-secondary">بیشتر</button>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($productGroup2 as $product)
                    <div class="col-12 col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top"
                                 src="{{asset('storage/'.$product->icon)}}"
                                 alt="Card image cap" width="140" height="140">
                            <div class="card-body">

                                <h5><a class="card-text" href="{{route('product.details' , ['product'=>$product->id] ) }}" role="button">{{$product->title}}</a></h5>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary"><a href="{{route('product.add_to_cart',$product->id)}}">خرید</a></button>
                                    </div>
                                    <small class="text-muted">{{$product->price.'تومان'}}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@endsection


