@extends('frontend.mainlayout')


@section('content')
    <div class="container marketing">
        <hr class="featurette-divider">

        <div class="row featurette">

            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="{{asset('storage/'.$product->icon)}}"
                     alt="Generic placeholder image">
            </div>

            <div class="col-md-7 d-flex flex-column justify-content-center">
                <h2 class="h1">{{$product->title}}</h2>
                <p class="lead">{{$product->description}}</p>
                <hr>
                <p class="lead">موجودی : {{$product->quantity}}</p>
                <h3 class="text-muted">{{'قیمت واحد : '.$product->price .' تومان '}}</h3>

                <form method="get" action="{{route('product.add_to_cart' , ['id'=>$product->id ])}}">
                    تعداد: <input class="form-control" type="text" name="quantity" style="width: 50px;height:25px;">
                    <div class="container">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <br>
                            <div class="btn-group">
                                <button type="submit" class="btn btn-sm btn-outline-secondary">
                                    <h6>افزودن به سبد خرید</h6></button>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>

            </div>
        </div>
    </div>


    <div class="container">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h5">موارد مشابه</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group ml-2">
                    <button class="btn btn-sm btn-outline-secondary">بیشتر</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach($productGroup2 as $product)
                <div class="col-12 col-md-3">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top"
                             src="{{asset('storage/'.$product->icon)}}"
                             alt="Card image cap" width="140" height="140">
                        <div class="card-body">

                            <h5><a class="card-text" href="{{route('product.details' , ['product'=>$product->id] )  }}"
                                   role="button">{{$product->title}}</a></h5>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary"><a  href="{{route('product.add_to_cart',$product->id)}}">خرید</a></button>
                                </div>
                                <small class="text-muted">{{$product->price.'تومان' }} </small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <hr class="featurette-divider">
    </div>
@endsection





