@extends('frontend.mainlayout')
@section('content')
    <div class="container fluid mt-100 ">
        <div class="col-md-9 col-sm-8 content container ">
            <div>
                <br><br>
            </div>
            @php
                $products = session()->get('cart');
            @endphp

            @if($products == null)
                <div class="container-fluid mt-100 ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 style="text-align: center">سبد خرید</h5>
                                </div>
                                <div class="card-body cart">
                                    <div class="col-sm-12 empty-cart-cls text-center"><img
                                            src="https://image.flaticon.com/icons/svg/2037/2037021.svg" width="150"
                                            height="150" class="img-fluid mb-4 mr-3">
                                        <h3><strong>سبد خرید شما خالی است!</strong></h3>
                                        <p>میتوانید برای افزودن محصول به سبد خرید خود به صفحه اصلی مراجعه کنید </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>

            @else


                <div class="container-fluid mt-100">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 style="text-align: center">سبد خرید</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                            <tr style="text-align: center">
                                                <th>محصول</th>
                                                <th>نام</th>
                                                <th>تعداد</th>
                                                <th>قیمت واحد</th>
                                                <th>جمع</th>
                                                <th>حذف محصول</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @php
                                            $sum=0;
                                            @endphp

                                            @foreach($products as $key => $product)

                                                {{$sum += $product['price']}}

                                                <tr style="text-align: center">
                                                    <td><img src="{{asset('storage/'.$product['icon'])}}"
                                                             class="img-cart"
                                                             style="width: 100px ; height: 100px;"></td>
                                                    <td><strong>{{$product['title']}}</strong>
                                                    <td>
                                                        <form class="form-inline">
                                                            <input class="form-control" type="number"
                                                                   style="width: 70px;" value={{$product['quantity']}}>
                                                        </form>
                                                    </td>
                                                    <td>{{$product['price']}} تومان</td>
                                                    <td>{{$product['price']*$product['quantity']}} تومان</td>


                                                    <td>
                                                        <a href="{{route('cart.delete' , ['id'=>$product['id']])}}"
                                                           class="btn btn-primary">
                                                            <i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                              <tr>
                                                  <td>{{$product['price']}} تومان</td>
                                              </tr>
                                            </tbody>

                                        </table>
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container " style="text-align: left">
                            <br>
                            <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp
                                پرداخت
                            </a>
                        </div>
                        <div>
                            <br>
                        </div>
                        @endif
                    </div>
                </div>
        </div>
    </div>
@endsection
