<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    public function getCategory(){
        return $this->belongsTo(Category::Class,'category_id');
    }
}
