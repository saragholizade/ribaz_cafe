<?php


namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Product;

class CartPageController extends Controller
{
    public function  index(){

        return view('frontend.CartPage');
    }

    public function delete($id){
        $cart= session()->get('cart');
        if($cart!=null) {
            unset($cart[$id]);
            session()->put('cart', $cart);
        }
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }
}
